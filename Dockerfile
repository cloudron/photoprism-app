FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=210111-cc05c430

# Install additional distribution packages
RUN apt-get update && apt-get install -y --no-install-recommends \
        wget \
        ca-certificates \
        sqlite3 \
        tzdata \
        libheif-examples \
        gnupg \
        gpg-agent \
        apt-utils \
        add-apt-key \
        exiftool \
        rawtherapee \
        ffmpeg

# Install RAW to JPEG converter
# RUN sh -c "echo 'deb http://download.opensuse.org/repositories/home:/pmdpalma:/Ubuntu:/20.04:/bucket/xUbuntu_20.04/ /' > /etc/apt/sources.list.d/home:pmdpalma:Ubuntu:20.04:bucket.list" && \
#     wget -qO - https://download.opensuse.org/repositories/home:pmdpalma:Ubuntu:20.04:bucket/xUbuntu_20.04/Release.key | apt-key add - && \
#     apt-get update && \
#     apt-get install darktable && \
#     apt-get upgrade && \
#     apt-get dist-upgrade && \
#     apt-get clean && \
#     rm -rf /var/lib/apt/lists/*

RUN curl -LSs https://github.com/photoprism/photoprism/archive/${VERSION}.tar.gz | tar -xz -C /app/code/ --strip-components 1 -f -
RUN make dep build-js install

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]

